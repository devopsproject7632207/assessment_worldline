<h1> step 1: creation of node js application</h1>
<p>a simple node js application is created to demonstrate the gitlab cicd pipeline. It consists of two web pages page1.html and page2.html which share a common text file info.txt</p>
<p>the info.txt gets created dyanamically which gets shared between the two pages</p>
<br>
<h1>step 2: gitlab repository set up </h1>
<p>the gitlab repository is set up as public and all the codes are pushed.</p>
<br>
<h1><step 3: gitlab pipeline creation/h1>
<p>gitlab pipeline is created to run the node js application.</p>
<br>
<h1>step 4: appropriate commits</h1>
<p>do run the application successfully appropriate commits are made like changes in app.js as well as .gitlab-ci.yml file</p>
<br>
<h1>step 5: the application is deployed on jenkins</h1>
<p>the application is successfully deployed on jenkins and runs successfully</p>
