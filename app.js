const express = require('express');
const fs = require('fs');
const path = require('path');

const app = express();
const PORT = 3000;

// Middleware to serve static files
app.use(express.static(path.join(__dirname, 'public')));

// Route to serve page1.html
app.get('/page1', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'page1.html'));
});

// Route to serve page2.html
app.get('/page2', (req, res) => {
    // Read the content of info.txt and send it to page2.html
    fs.readFile(path.join(__dirname, 'data', 'info.txt'), 'utf8', (err, data) => {
        if (err) {
            console.error(err);
            return res.status(500).send('Error reading info file');
        }
        res.send(data);
    });
});

// Create info.txt file
fs.writeFileSync(path.join(__dirname, 'data', 'info.txt'), 'Hello, this is info.txt content!', 'utf8');

// Server
app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});
